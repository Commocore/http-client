﻿## This repository contains:

- original Http-Load library written by Doc Bacardi / The Dreams
- custom POST method add-on library written by Bartosz Żołyński / Commocore (based on the original Http-Load), which enables sending custom body requests
- DHCP taken from Contiki to detect dynamic IP address (FYI. in Contiki 2016 version, it was renamed to `IPCONFIG`)
- examples for GET and POST methods, and some debugging options


## About HTTP Client

The HTTP Client enables sending requests via HTTP on Commodore 64/128 machines. It currently supports GET and POST requests. HTTP Client was tested with the 64NIC+ (RR-Net type) ethernet card.
HTTP Client uses 6502 JSON Parser library to parse JSON responses from API: [https://bitbucket.org/Commocore/6502-json-parser](https://bitbucket.org/Commocore/6502-json-parser)

The project is under development. At the moment, it is used to test the online features of the [CommodoRex game](https://commocore.com/commodorex/).


## Installation

HTTP Client uses [6502 JSON Parser library](https://bitbucket.org/Commocore/6502-json-parser) which needs to be installed as a vendor dependency.
To install, simply run `6502-json-parser-dependency.bat` script (Windows), or `6502-json-parser-dependency.sh` (*nix) to clone it from a remote git repository.


## Configuring IPCONFIG-DEFAULT

`IPCONFIG-DEFAULT` is a configuration file used for example by Singular web browser.
This file is loaded-on-start-up of the HTTP Client (disk drive #8), and if not found, default settings from the main program are used.

1. Use `DHCP` available also from Contiki (renamed to `IPCONFIG` for Contiki 2016). In this repository, you'll find it in the `tools/dhcp` folder.
Run it to obtain a dynamic IP address and other addresses (Gateway, Network and DNS server). Use F5/F7 and RETURN key to Request IP Address.
Write it down, we'll need it to configure `IPCONFIG-DEFAULT`.
Saving in `DHCP` will update the `CONTIKI.CFG` so it's not necessary in our case.
2. Then, use `SETMAC` available also on `64nic.d64` disk compilation. In this repository, you'll find it in the `tools` folder.
It will check MAC address of your device. Write it down. Note, it's provided in hex values.
3. It's time to build the `IPCONFIG-DEFAULT` configuration file with all the information you've gathered. You have two choices:
	1. Manually compile the settings file `IPCONFIG-DEFAULT` with all the details.
	In order to do it, copy file `src/ipconfig-default.asm.dist` to `src/ipconfig-default.asm`.
	Then, set up addresses and compile the file using either `compile-ipconfig-default.bat` script (Windows), or `compile-ipconfig-default.sh` script (*nix).
	2. You can also load `IPCONFIG` BASIC file, available also from Singular web browser. In this repository you'll find it in the `tools` folder.
	Note the output device in line 10 (`PEEK(186)` will return the last used device) and no ability to overwrite the previous configuration in line 10.
	Then, provide MAC address for the `ADAPTER` (note: translate all hex values to decimal), and other addresses. Don't touch the first two bytes in each DATA line
	as they are used as the headers for each address.


## Building HTTP POST body requests

In order to build data form, a few additional functions have been provided. See the example below:


```
	#HttpPostMethod.resetBody ; to ensure that we're building data from the beginning
	#HttpPostMethod.setBodyMemoryLocation HTTP_POST_METHOD_BODY_REQUEST_LOCATION

	; Add key for the first name to request body
	#HttpPostMethod.addStringToBody firstNameKey, firstNameKeyLength
	
	; Add value of the first name to request body
	ldx #0
-
	lda firstName,x
	cmp #END_OF_STRING_CHAR
	beq +
		#HttpPostMethod.addByteToBody
		inx
		jmp -
+
	
	; Add key for last name to request body
	#HttpPostMethod.addStringToBody lastNameKey, lastNameKeyLength

	; Add value of last name to request body
	ldx #0
-
	lda lastName,x
	cmp #END_OF_STRING_CHAR
	beq +
		#HttpPostMethod.addByteToBody
		inx
		jmp -
+
rts


.enc "ascii"
.cdef 32, 126, 32 ; identity translation 32-126 to 32-126

firstNameKey
	.text "firstName="
firstNameKeyLength = * - firstNameKey


lastNameKey
	.text "&lastName=" ; note "&" used here to glue another parameter
lastNameKeyLength = * - lastNameKey


.enc "none"


firstName
	.fill 32, 0 ; e.g. data can be typed during the execution of the program


lastName
	.fill 32, 0 ; e.g. data can be filled from the previous API response
	

```

Note: you can also use this to build query string parameters for GET HTTP method
by setting the `setBodyMemoryLocation` at the end of the URL.


## Compiling

Use `compile.bat` for Windows, or `compile.sh` for *nix.
		

## Notes

Total response time faster than 200ms might lead to HTTP Client hangs.
This is because the HTTP Client program may not be ready to receive a response yet.
A safety net is to set a delay of at least 200ms.

E.g. in PHP it might be achieved this way:

```php
usleep(200000);
```


## License.

Copyright © 2017 - 2019, Bartosz Żołyński, [Commocore](http://www.commocore.com).

Released under the [GNU General Public License, version 3](LICENSE).
