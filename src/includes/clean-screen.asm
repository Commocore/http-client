
; @access public
; @return void
cleanScreen
	lda #0
	sta $d020
	sta $d021
		
	lda #32
	ldx #250
-
	sta SCREEN_LOCATION + 250 * 0 - 1,x
	sta SCREEN_LOCATION + 250 * 1 - 1,x
	sta SCREEN_LOCATION + 250 * 2 - 1,x
	sta SCREEN_LOCATION + 250 * 3 - 1,x
	dex
	bne -
rts
