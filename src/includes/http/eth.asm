
Eth_Proto_Ip			= $0800
Eth_Proto_Arp			= $0806
Eth_Proto_PPPoED		= $8863


;	.segment "data"
	.comment
EthRec_SrcMac			*=*+6
	.endc
;    .(

;--------------------------------------
; wait 1 second for a packet

EthCfg_Timeout		= 1

;--------------------------------------

Eth2Header_DstMac	= $00
Eth2Header_SrcMac	= $06
Eth2Header_Typ		= $0c
Eth2Header_length	= $0e

;--------------------------------------

eth_setProtocol .proc
		sta Eth_Proto
		sty Eth_Proto+1
		rts
    		.pend

;--------------------------------------

eth_getPacket .proc
	ldx #<EthZp_TimeOutBuffer
	ldy #>EthZp_TimeOutBuffer
	jsr timer_get

eth_getPacket0
	jsr Phy_get
	bcs error0
	cpx #$ff
	bne getPacket0

        brk
        brk

	lda #EthCfg_Timeout
	ldx #<EthZp_TimeOutBuffer
	ldy #>EthZp_TimeOutBuffer
	jsr timer_check
	bcc eth_getPacket0

error0
	sec
	rts

getPacket0
;	stx EthZp_BucketIn
	jsr Bucket_GetAdr

	;all known types start with 8
	ldy #Eth2Header_Typ
	lda (BucketZp_BufPtr),y
        cmp #8
        bne ignorePacket
	iny
	lda (BucketZp_BufPtr),y
	cmp #<Eth_Proto_Ip
	beq ipPacket
	cmp #<Eth_Proto_Arp
	beq arpPacket

ignorePacket
;	ldx EthZp_BucketIn
	jsr Bucket_Free
;	bcs error0
	jmp eth_getPacket0	; strange typ code -> ignore Packet


ipPacket
	jsr cutInPacket
;	bcs error0
	jmp ipv4_getPacket

arpPacket
	jsr cutInPacket
;	bcs error0
	jmp arp_getPacket

cutInPacket
;	ldx EthZp_BucketIn
	ldy #Eth2Header_length
	jmp Bucket_ShrinkFront
        .pend

;--------------------------------------
; Name:
;   eth_sendPacket
;
; Description:
;   Encapsule the data in an ethernet 2 packet and pass it down to the physical layer.
;
; Parameters:
;   X : Bucket index of the data
;
; Returns:
;   Carry : 0 = ok, packet send
;           1 = error

eth_sendPacket .proc
;	stx EthZp_BucketIn

	lda #Eth2Header_length
	jsr Bucket_GrowFront_small
;	bcs error
;	stx EthZp_BucketOut
	jsr Bucket_GetAdr
;	bcs error

	;Set Typ
	ldy #Eth2Header_Typ+1
	lda Eth_Proto+1
	sta (BucketZp_BufPtr),y
	dey
	lda Eth_Proto
	sta (BucketZp_BufPtr),y
	dey

	;Set Src Mac
copySrcMac
	lda MyMacAdr-Eth2Header_SrcMac,y
	sta (BucketZp_BufPtr),y
	dey
	cpy #Eth2Header_SrcMac
	bge copySrcMac

	;Set Dst Mac
copyDstMac
	lda Eth_DstMac,y
	sta (BucketZp_BufPtr),y
	dey
	bpl copyDstMac

;	ldx EthZp_BucketOut
	gmi Phy_send

error
	sec
	rts
    .pend

;--------------------------------------

;    .)


