
;--------------------------------------

	; fixed

HttpZp_DataPtr			= $ae

;	.segment "data"
	.comment
HttpZp_PacketLength		.DW 0
HttpZp_DataLength		.DW 0
HttpZp_State			.DB 0
HttpZp_BucketOut		.DB 0
Http_Line			.DSB 256
HttpZp_LineLen			.DB 0
HttpZp_Num			.DW 0
HttpZp_NumLen			.DB 0
HttpZp_BlkLen			.DB 0
	.endc
;--------------------------------------

;	.segment "bank0"

HttpState_Idle			= 0
HttpState_Open			= 1

;--------------------------------------

http_init
	lda #HttpState_Idle
	sta HttpZp_State
	rts

;--------------------------------------

http_execute .proc
        lda #10
        sta maxredirect

restart	lda $d012;#<32772
	ldx #>32772
	jsr tcp_setSrcPort

	lda #<80
	ldx #>80
	jsr tcp_setDstPort

	lda #HttpState_Open
	sta HttpZp_State

	ldy DstIpIdx
	jsr tcp_connect
	bcs http_error

	; send the request
	jsr http_sendGet
	bcs http_error

	jsr http_getLine
	bcc http_testReply
http_error
	sec
	rts

http_testReply
	lda HttpZp_LineLen
	cmp #14
	bcc notFound

	ldx #header0_len-1
test0
	lda header0,x
	cmp Http_Line,x
	bne notFound
	dex
	bpl test0

	; test for 0 or 1
	lda Http_Line+header0_len
	and #$fe
	cmp #"0"
	bne notFound

	ldx #header1_len-1
-	lda header1,x
	cmp Http_Line+header0_len+1,x
	bne notOK
	dex
	bpl -

-	jsr http_getLine
	bcs notFound
	txa
	bne -

	clc
	rts
notOK
        dec maxredirect
        beq notFound

	ldx #header2_len-1
-	lda header2,x
	cmp Http_Line+header0_len+1,x
	bne notFound
	dex
	bpl -

lp      jsr http_getLine
	bcs notFound
        txa
        beq notFound
        sta u+1

	ldx #header3_len-1
-	lda header3,x
	cmp Http_Line,x
	bne lp
	dex
	bpl -

        ldy #0
        ldx #header3_len
-       lda Http_Line,x
        sta fileName,y
        iny
        inx
u       cpx #0
        blt -
        sty fileName_len

        jsr tcp_disconnect2

        jsr parseHostName
        jmp restart

notFound
	;TODO: test for other reply codes and generate a more reasonable errormessage than "file not found"

	sec
	rts

header0
	.text "HTTP/1."
header0_len = * - header0

header1
	.text " 200"
header1_len = * - header1

header2
	.text " 30"
header2_len = * - header2

header3
	.text "Location: http://"
header3_len = * - header3

maxredirect     .byte 0
        .pend

;--------------------------------------

http_sendGet .proc
	lda #<(http_getCmd0_length+http_getCmd1_length+http_getCmd2_length)
	clc
	adc fileName_len
	pha
	lda #>(http_getCmd0_length+http_getCmd1_length+http_getCmd2_length)
	adc #0
	tay
        pla

	jsr Bucket_Alloc
	bcs error0
	stx HttpZp_BucketOut

	ldy #http_getCmd0_length-1
copyGet0
	lda http_getCmd0,y
	sta (BucketZp_BufPtr),y
	dey
	bpl copyGet0

	lda BucketZp_BufPtr
	clc
	adc #http_getCmd0_length
	sta BucketZp_BufPtr
        bcc +
	inc BucketZp_BufPtr+1
+
        ldx HostNameLength
	ldy #0
-	lda fileName,x
	sta (BucketZp_BufPtr),y
	iny
        inx
	cpx fileName_len
	bcc -

        tya
        clc
	adc BucketZp_BufPtr
	sta BucketZp_BufPtr
        bcc +
	inc BucketZp_BufPtr+1
+

	ldy #http_getCmd1_length-1
-	lda http_getCmd1,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -

	lda BucketZp_BufPtr
	clc
	adc #http_getCmd1_length
	sta BucketZp_BufPtr
        bcc +
	inc BucketZp_BufPtr+1
+
	ldy #0
-	lda fileName,y
	sta (BucketZp_BufPtr),y
	iny
	cpy HostNameLength
	bcc -

        tya
        clc
	adc BucketZp_BufPtr
	sta BucketZp_BufPtr
        bcc +
	inc BucketZp_BufPtr+1
+
	ldy #http_getCmd2_length-1
-	lda http_getCmd2,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -

	ldx HttpZp_BucketOut
	jmp tcp_sendData

error0
	sec
	rts


http_getCmd0
	.text "GET "
http_getCmd0_length = *-http_getCmd0

http_getCmd1
	.text " HTTP/1.0",$0d,$0a
	.text "User-Agent: "
	.text httpUserAgent
	.text " ("
        .if SUPERRAM
        .text "SuperCPU "
        .fi
        .if C128
        .text "Commodore 128"
        .else
        .text "Commodore 64"
        .fi
        .text")",$0d,$0a
	.text "Accept: text/html,text/plain;q=0.8,*/*;q=0.5",$0d,$0a
        .text "Host: "
http_getCmd1_length = *-http_getCmd1

http_getCmd2
	.text $0d,$0a,"Connection: close",$0d,$0a
	.text $0d,$0a
http_getCmd2_length = *-http_getCmd2
	.pend

;--------------------------------------
; Get Line reads the Http header text
; Carriage Returns are ignored,
; all letters are converted to the normal c64 range ($41-$5a).
; A header line is terminated by a Linefeed

http_getLine .proc
	tsx
        stx getbyte.vg+1

	ldx #0
getLoop
	jsr getbyte
	bcs error
	cmp #$0d
	beq getLoop
	cmp #$0a
	beq gotLf
	sta Http_Line,x
	inx
	bne getLoop

	;line too long!
error
	sec
	rts

gotLf
	stx HttpZp_LineLen
	clc
	rts
    .pend

