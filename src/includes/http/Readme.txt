
Http-Load

Where to get
------------

The latest version of Http-load is waiting for your download in my little
dreamland:

<http://people.freenet.de/LittleDreamLand/httpload2.html>


Requirements
------------

You will need these tools to build Http-Load:

 * the usual linux dev stuff like gcc, make etc.
 
 * dreamass
   Get it from <http://people.freenet.de/LittleDreamLand/dreamass>.
 
 * pucrunch (only if you are building the prg version)



The Output
----------

Type 'make' in the directory which contains this Readme.txt. Ignore the
strange messages. 2 files will be build:

  http64.bin : this is the rom, use it with the retro replay's flashtool

  hp         : a ram version of http-load. this is only interesting for
               debugging as most of the ram above $8000 will be used. No
	       retro replay ram is used to allow debugging with a freezer.


History
-------

2004-09-19
	* Bugfix: Packets were never send via the gateway.
	* Bugfix: Arp requests from another host failed.
	* Bugfix: Interpreter for dns replies was much too picky.
	* and some smaller stuff...

	* New: timeout and retry for dns operations


2004-07-14
	* Bugfix: 100 and 200 were printed as "1 0" / "2 0" in the setting gui.
	  Thanks to Monte Carlos for the bugreport!


2004-07-07
	Many changes and fixes from Http-Load V1, the most important ones are:

	* Settings menu instead of ugly sys configuration
	* dns routines
	* ip cache
	* arp cache
	* faster memory management routines
	* arp replies were always send to the gateway instead of the sender.
	  Thanks to Jaymz Julian for the bugfix!
	* and maaaany little bugs fixed :)


Thanks
------

* Many thanks go to Jaymz Julian for fixing a bug in the arp code!
* Monte Carlos found a bug in the settings menu.

--
Doc Bacardi/The Dreams

