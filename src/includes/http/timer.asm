
;	.segment "zp"
	.comment
timerZp_Ptr			.word 0
	.endc

;	.segment "data"
	.comment
timer_A				.byte 0
timer_BufNow			*=*+4
timer_BufMax			*=*+4
	.endc

;	.segment "bank0"

;--------------------------------------
; Set clock to 0. Not really necessary...

timer_init .proc
	lda #0
	sta $dd0f
	sta $dd0b
	sta $dd0a
	sta $dd09
	sta $dd08
	rts
        .pend

;--------------------------------------
; Get Timer
; Parameters:
;   X/Y : Lo/Hi byte of pointer to 4 byte buffer for timer

timer_get .proc
	pha
	stx timerZp_Ptr
	sty timerZp_Ptr+1

	ldy #3
-	lda $dd08,y
	sta (timerZp_Ptr),y
	dey
	bpl -

	pla
	; X was not modified
	ldy timerZp_Ptr+1
	rts
        .pend

;--------------------------------------
; Check for timeout
; Parameters:
;   A   : BCD Timeout in seconds
;   X/Y : Lo/Hi byte of pointer to 4 Bytes
; Return:
;   Carry : 0 = no timeout, 1 = timeout

timer_check .proc
	sta timer_A
	stx timerZp_Ptr
	sty timerZp_Ptr+1

	; get timer (*must* read from $0b downto $08)
	ldy #3
timer_check0
	lda $dd08,y
	sta timer_BufNow,y
	dey
	bpl timer_check0

	; add timeout seconds to time in buffer from the calling routine
	ldy #1
	sed
	clc
	lda (timerZp_Ptr),y		; seconds
	adc timer_A
	sta timer_BufMax+1
	iny
	lda (timerZp_Ptr),y		; minutes
	adc #0
	sta timer_BufMax+2
	iny
	lda (timerZp_Ptr),y		; hours
	and #$1f			; no am/pm flag
	adc #0
;	sta timer_BufMax+3

	; correct values in case of overflow (>60 for second and minutes, >12 for hours)
timer_check1
;	lda timer_BufMax+3		; test hours for overflow
	cmp #$12			; must be < 12
	bcc timer_check2x
	sbc #$12
;	sta timer_BufMax+3
	bcs timer_check1
timer_check2x
	sta timer_BufMax+3

timer_check2
	lda timer_BufMax+2		; test minutes for overflow
	cmp #$60			; must be < 60
	bcc timer_check3
	sbc #$60
	sta timer_BufMax+2
	lda timer_BufMax+3		; hours
	adc #0
;	sta timer_BufMax+3
	bcs timer_check1		; check again

timer_check3
	lda timer_BufMax+1		; test seconds for overflow
	cmp #$60			; must be < 60
	bcc timer_check4
	sbc #$60
	sta timer_BufMax+1
	lda timer_BufMax+2
	adc #0
	sta timer_BufMax+2
	lda timer_BufMax+3		; hours
	adc #0
;	sta timer_BufMax+3
	bcs timer_check1

	; substract 'max' from 'now', if 'now' is equal or greater
	; than 'max', it's a timeout
timer_check4
	ldx #0
	ldy #3
	sec
timer_check5
	lda timer_BufNow,x
	sbc timer_BufMax,x
	inx
	dey
	bpl timer_check5

	cld
	lda timer_A
	ldx timerZp_Ptr
	ldy timerZp_Ptr+1
	rts
        .pend

;--------------------------------------

