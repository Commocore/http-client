;--------------------------------------
; Physical network layer
;
; This is just a statemachine wrapper around the
; physical driver.
;--------------------------------------




;--------------------------------------
; Index for the Phy_init function to select
; the desired physical driver.
;
; NOTE: the numbers must match with the
; PhyDrivers_Install_lo and _hi tables.

PhyDriver_RRNet			= 0

;--------------------------------------
; Global jump table for the driver
;	.segment "data"
	.comment
PhyZp_MacAdr                    *=*+6

PhyJmp_Open			*=*+3
PhyJmp_Close			*=*+3
PhyJmp_GetPacket		*=*+3
PhyJmp_SendPacket		*=*+3
	.endc
;    .(

;--------------------------------------
; States of the physical layer

PhyState_NoDriver		= 0
PhyState_Closed			= 1
PhyState_Open			= 2

;--------------------------------------

;	.segment "zp"


;	.segment "data"
	.comment
PhyZp_InitJmp			*=*+3

PhyZp_State			.byte 0
	.endc
;	.segment "bank0"

;--------------------------------------

PhyDrivers_Install_lo
	.byte <cs8900a_rrnet_install
PhyDrivers_Install_length = * - PhyDrivers_Install_lo

PhyDrivers_Install_hi
	.byte >cs8900a_rrnet_install

;--------------------------------------
; Name:
;   Phy_init
;
; Description:
;   Init the pysical layer.
;
; Parameters:
;   -
;
; Returns:
;   -

Phy_init .proc
	lda #PhyState_NoDriver
	sta PhyZp_State

	ldx #2
copyDummy
	lda dummyJmp,x
	sta PhyJmp_Open,x
	sta PhyJmp_Close,x
	sta PhyJmp_GetPacket,x
	sta PhyJmp_SendPacket,x
	dex
	bpl copyDummy

	rts

dummyJmp
	jmp SecureDummy

SecureDummy
	sec
	rts
    .pend

;--------------------------------------
; Name:
;   Phy_installDriver
;
; Description:
;   Install a driver for the physical layer.
;
; Parameters:
;   X     : index of the physical driver, e.g. PhyDriver_RRNet
;
; Returns:
;   Carry : 0 = init ok, physical device detected and initialized
;               NOTE: this does not mean that a link is established!
;           1 = error

Phy_installDriver .proc
	; open driver is only allowed at state PhyState_NoDriver
	lda PhyZp_State
	cmp #PhyState_NoDriver
	bne illegalState

	; is the drivers index valid?
	cpx #PhyDrivers_Install_length
	bcs unknown_driver

	; get init routine for the driver
	lda #$4c
	sta PhyZp_InitJmp
	lda PhyDrivers_Install_lo,x
	sta PhyZp_InitJmp+1
	lda PhyDrivers_Install_hi,x
	sta PhyZp_InitJmp+2

	jsr PhyZp_InitJmp
	bcs no_device

	; device found and vectors installed
	lda #PhyState_Closed
	sta PhyZp_State
	rts

illegalState
unknown_driver
no_device
	sec
	rts

    .pend

;--------------------------------------
; Name:
;   Phy_open
;
; Description:
;   Assign a mac address to the device and establish the link
;
; Parameters:
;   X : lo ptr to 6 bytes mac address
;   Y : hi ptr to 6 bytes mac address
;
; Returns:
;   Carry : 0 = ok, link established
;           1 = error, look at Status for details

Phy_open .proc
	; only allowed in state PhyState_Closed
	lda PhyZp_State
	cmp #PhyState_Closed
	bne illegalState

	; abuse BucketZp_BufPtr for the ip pointer
	stx BucketZp_BufPtr
	sty BucketZp_BufPtr+1

	; copy mac address
	ldy #5
copyMac
	lda (BucketZp_BufPtr),y
	sta PhyZp_MacAdr,y
	dey
	bpl copyMac

	; try to establish the link
	jsr PhyJmp_Open
	bcs error

	; update the state machine
	lda #PhyState_Open
	sta PhyZp_State
	rts

illegalState
error
	sec
	rts
    .pend

;--------------------------------------
; Name:
;   Phy_close
;
; Description:
;   Close the link.
;
; Parameters:
;   -
;
; Returns:
;   Carry : 0 = ok, link closed
;           1 = error, look at Status for details

Phy_close .proc
	; only allowed in state PhyState_Open
	lda PhyZp_State
	cmp #PhyState_Open
	bne illegalState

	; try to close the link
	jsr PhyJmp_Close
	bcs error

	; update the state machine
	lda #PhyState_Closed
	sta PhyZp_State
	rts

illegalState
error
	sec
	rts
    .pend

;--------------------------------------
; Name:
;   Phy_get
;
; Description:
;   Poll for a waiting packet, return immediately if no packet is waiting.
;
; Parameters:
;   -
;
; Returns:
;   Carry : 0 = ok, data sent
;           1 = error, look at Status for details
;   X     : $ff for no packet,
;           else the bucket index holding the received data.

Phy_get .proc
	; only allowed in state PhyState_Open
	lda PhyZp_State
	cmp #PhyState_Open
	bne illegalState

	jmp PhyJmp_GetPacket

illegalState
	sec
	rts
    .pend


;--------------------------------------
; Name:
;   Phy_send
;
; Description:
;   Send a packet.
;
; Parameters:
;   X     : index to the bucket containing the data to send
;
; Returns:
;   Carry : 0 = ok, data sent
;           1 = error, look at Status for details

Phy_send .proc
	; only allowed in state PhyState_Open
	lda PhyZp_State
	cmp #PhyState_Open
	bne illegalState

	jmp PhyJmp_SendPacket

illegalState
	sec
	rts
    .pend

;--------------------------------------

;    .)

