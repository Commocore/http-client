

;--------------------------------------

DnsCfg_Timeout		= 5
DnsCfg_RetryNum		= 5

;--------------------------------------

DnsState_Idle		= 0
DnsState_Request	= 1

;--------------------------------------

; Zp shares with memcopy
DnsZp_Ptr	= CopyZp_Src

;	.segment "data"
	.comment
DnsZp_BucketIn			.DB 0
DnsZp_BucketOut			.DB 0

DnsZp_State			.DB 0

DnsZp_TransactionCnt		.DW 0

; Datablock for one request
DnsZp_Transaction		.DW 0
DnsZp_BucketRequest		.DB 0
DnsZp_RequestLen		.DB 0

DnsZp_Retries			.DB 0
DnsZp_TimeOutBuffer		.DSB 4

dnsResult			.DSB $10
	.endc
;--------------------------------------

;	.segment "bank0"

dns_init .proc
	lda #DnsState_Idle
	sta DnsZp_State

	; TODO: generate a random transaction number instead of the fixed $d00f
	lda $dc04  	;mooh!
	sta DnsZp_TransactionCnt
	lda $dc05
	sta DnsZp_TransactionCnt+1

	rts
    .pend


dns_getIp .proc
	stx DnsZp_BucketRequest

	inc DnsZp_TransactionCnt
	bne +
	inc DnsZp_TransactionCnt+1
+

	lda #DnsCfg_RetryNum
	sta DnsZp_Retries
	lda #DnsState_Request
	sta DnsZp_State

resendReq
	jsr dns_sendRequest
	bcs error

	ldx #<DnsZp_TimeOutBuffer
	ldy #>DnsZp_TimeOutBuffer
	jsr timer_get

getPacket
	; receive tcp packet from MyDnsIpIdx
	ldy MyDnsIpIdx
	jsr udp_get
	bcs notFound

	; check state to see if an arp packet was received
	lda DnsZp_State
	cmp #DnsState_Idle
	beq ok

notFound
	lda #DnsCfg_Timeout
	ldx #<DnsZp_TimeOutBuffer
	ldy #>DnsZp_TimeOutBuffer
	jsr timer_check
	bcc getPacket

	dec DnsZp_Retries
	bne resendReq

error
	sec
	rts

ok
	clc
	rts
    .pend


dns_sendRequest .proc

	; get the length of the encoded hostname
	ldx DnsZp_BucketRequest
	jsr Bucket_Len
	; hostname > 255 chars?
	cpy #1
	bcs dns_hostnameError
	sta DnsZp_RequestLen

	; add size of the dns request header
	adc #2+dnsSend_fixedPreData_len
	bcc +
	iny
+
	; and allocate a new buffer.
	; don't grow the hostname buffer, we need to keep it!
	jsr Bucket_Alloc
	bcs dns_BucketError
	stx DnsZp_BucketOut

	; the new buffer is the destination of the copy operation
	jsr Bucket_GetAdr
	clc
	adc #2+dnsSend_fixedPreData_len
	sta CopyZp_Dst
	tya
	adc #0
	sta CopyZp_Dst+1

	; source is the request buffer
	ldx DnsZp_BucketRequest
	jsr Bucket_GetAdr
	sta CopyZp_Src
	sty CopyZp_Src+1

	jsr Bucket_Len
	sta CopyZp_Len
	sty CopyZp_Len+1

	; Copy the request
	jsr MemCopy

	ldx DnsZp_BucketOut
	jsr Bucket_GetAdr

	; copy transaction nr
	ldy #0
        ldx #1
-	lda DnsZp_TransactionCnt,x		; hi byte first
	sta DnsZp_Transaction,x
	sta (BucketZp_BufPtr),y
	iny
        dex
        bpl -

	; copy fixed request header
	ldx #dnsSend_fixedPreData_len-1
	ldy #2+dnsSend_fixedPreData_len-1
copyFixedPreDnsReq
	lda dnsSend_fixedPreData,x
	sta (BucketZp_BufPtr),y
	dey
	dex
	bpl copyFixedPreDnsReq

	lda BucketZp_BufPtr
	ldy BucketZp_BufPtr+1
	clc
	adc #2+dnsSend_fixedPreData_len
	bcc +
	clc
	iny
+	adc DnsZp_RequestLen
	bcc +
	iny
+	sta BucketZp_BufPtr
	sty BucketZp_BufPtr+1

	lda #<32768
	ldx #>32768
	jsr udp_setSrcPort
	lda #<53
	ldx #>53
	jsr udp_setDstPort

	ldx DnsZp_BucketOut
	ldy MyDnsIpIdx
	jmp udp_send

dns_hostnameError
dns_BucketError
	sec
	rts

dnsSend_fixedPreData
	.byte $01, $00	; flags (standard query)
	.byte $00, $01	; 1 query
	.byte $00, $00	; 0 answer rr's
	.byte $00, $00	; 0 authority rr's
	.byte $00, $00	; 0 additional rr's
dnsSend_fixedPreData_len = * - dnsSend_fixedPreData

    .pend

;--------------------------------------

dns_getPacket .proc
	stx DnsZp_BucketIn

	; did we request a dns packet? if not, discard it
	lda DnsZp_State
	cmp #DnsState_Request
	bne badDnsPacket

	jsr Bucket_GetAdr
	clc
	; generate pointer to the encoded hostname (for later compare)
	adc #12
	sta DnsZp_Ptr
	tya
	adc #0
	sta DnsZp_Ptr+1

	; test transaction nr, is this the reply for our request?
	ldy #0
	lda (BucketZp_BufPtr),y
	cmp DnsZp_Transaction+1
	bne badDnsPacket
	iny
	lda (BucketZp_BufPtr),y
	cmp DnsZp_Transaction
	bne badDnsPacket

	; test flags
	iny
	lda (BucketZp_BufPtr),y
	and #$fb			; do not care about authoritative flag
	cmp #$81
	bne badDnsPacket
	iny
	lda (BucketZp_BufPtr),y
	and #$8f
	cmp #$80
	bne badDnsPacket

	; test number of questions, must be 1
	iny
	lda (BucketZp_BufPtr),y
	bne badDnsPacket
	iny
	lda (BucketZp_BufPtr),y
	cmp #1
	bne badDnsPacket

	; test number of replies, must be >= 1
	iny
	lda (BucketZp_BufPtr),y
	iny
	ora (BucketZp_BufPtr),y
	beq dnsResolveError			;unknown hostname
        lda (BucketZp_BufPtr),y
        sta dnsResult

	; test query
	ldx DnsZp_BucketRequest
	jsr Bucket_GetAdr

	ldy DnsZp_RequestLen
testReplyQuery
	dey
	cpy #$ff
	beq queryOk
	lda (BucketZp_BufPtr),y
	cmp (DnsZp_Ptr),y
	beq testReplyQuery

badDnsPacket
bucketError
	sec
	rts

queryOk
	; move DnsZp_Ptr to first Answer
        lda DnsZp_RequestLen
donext  clc
	adc DnsZp_Ptr
	sta DnsZp_Ptr
	bcc +
	inc DnsZp_Ptr+1
+

	ldy #0
	lda (DnsZp_Ptr),y
        beq ok
        inc DnsZp_Ptr
        bne +
        inc DnsZp_Ptr+1
+	cmp #$c0
	blt donext
ok
-       iny
	lda (DnsZp_Ptr),y
	cmp dnsAnswerRR_fixed-1,y
	bne next ;Continue with next Answer
        cpy #4
	blt -

	ldy #11
-       lda (DnsZp_Ptr),y
        sta dnsResult-11,y
        iny
        cpy #15
        blt -

	; ok, free incoming bucket
	ldx DnsZp_BucketIn
	jsr Bucket_Free

	lda #<dnsResult
	ldx #>dnsResult
	jsr ipCache_New
	sty DstIpIdx
	bcs dns_recError

	lda #DnsState_Idle
	sta DnsZp_State
	rts

next    dec dnsResult
        beq badDnsPacket
        ldy #10
        tya
        sec
        adc (DnsZp_Ptr),y
        jmp donext

dnsResolveError
dns_recError
	sec
	rts

dnsAnswerRR_fixed
	.byte $00, $01	; Typ Host Address
	.byte $00, $01	; Class INet

    .pend

;--------------------------------------


