
initLayers .proc
 ;       lda #Error_Ok
;	sta Status

	;init all layers
	jsr Bucket_Init
	jsr ipCache_Init
	jsr timer_init
	jsr Phy_init

	;copy tmp mac to my mac
	ldx #5
copyMyMac
	lda ActCfg_Mac,x
	sta MyMacAdr,x
	dex
	bpl copyMyMac

	; init rrnet driver
	ldx #PhyDriver_RRNet
	jsr Phy_installDriver
	bcs error

	ldx #<MyMacAdr
	ldy #>MyMacAdr
	jsr Phy_open
	bcs error
.comment
	// ppp not yet finished...
	jsr ppp_init
	jsr pppoe_init

	jsr pppoe_discover
	bcs error

	jsr ppp_open
	bcs error
.endc
	; copy my ip to cache
	lda #<ActCfg_Ip
	ldx #>ActCfg_Ip
	jsr ipCache_New
	sty MyIpIdx
	bcs error

	; copy netmask
	lda #<ActCfg_NetMask
	ldx #>ActCfg_NetMask
	jsr ipCache_New
	sty MyNetMaskIdx
	bcs error

	lda #<ActCfg_Router
	ldx #>ActCfg_Router
	jsr ipCache_New
	sty MyGatewayIdx
	bcs error

	; set dns server's ip
	lda #<ActCfg_Nameserver
	ldx #>ActCfg_Nameserver
	jsr ipCache_New
	sty MyDnsIpIdx

	jsr arp_init
	jsr dns_init
	jsr tcp_init
	jsr http_init

	clc
	rts

error
	sec
	rts
    .pend

;--------------------------------------



