;--------------------------------------
; Physical driver for the RRNet adapter.
; Call cs8900a_rrnet_install to detect rrnet and install
; the driver.
;--------------------------------------

;    .(

;--------------------------------------
; Some Configuration

; time in seconds to wait for link pulses
CsCfg_OpenTimeout = 2

;--------------------------------------

;	.segment "data"
	.comment
CsZp_PacketLen			.word 0
CsZp_Bucket			.byte 0

CsZp_TimeOutBuffer		*=*+4
	.endc
;	.segment "bank0"

;--------------------------------------
; IO mapped registers
; note that registers 0-6 and 8-e are swapped. this maps the interrupt status
; register to $de00/$de01. both are used by the RetroReplay and are lost for
; the ethernet card. as irqs can not be used in 8 Bit mode, this is no big
; deal. without swapping the registers, RxTxPort0 would have been lost

                .if RR_NET
CS_PacketPage	= CS8900_BASE + 2
CS_PacketData0	= CS8900_BASE + 4
CS_PacketData1	= CS8900_BASE + 6
CS_RxTxPort0	= CS8900_BASE + 8
CS_RxTxPort1	= CS8900_BASE + 10
CS_TxCmd	= CS8900_BASE + 12
CS_TxLength	= CS8900_BASE + 14
                .else
CS_PacketPage	= CS8900_BASE + 2  + 8
CS_PacketData0	= CS8900_BASE + 4  + 8
CS_PacketData1	= CS8900_BASE + 6  + 8
CS_RxTxPort0	= CS8900_BASE + 8  - 8
CS_RxTxPort1	= CS8900_BASE + 10 - 8
CS_TxCmd	= CS8900_BASE + 12 - 8
CS_TxLength	= CS8900_BASE + 14 - 8
                .fi

;--------------------------------------
; the packet page registers
; write the number to CS_PacketPage and read/write data to CS_PacketData0.
; the values are divided by two to work "out of the box" with my cs_readPage
; and cs_writePage routines

CSPP_PId	= $0000/2
CSPP_RxCfg	= $0102/2
CSPP_RxEvent	= $0124/2
CSPP_RxCtl	= $0104/2
CSPP_TxCfg	= $0106/2
CSPP_TxEvent	= $0128/2
CSPP_TxCmd	= $0108/2
CSPP_BufCfg	= $010a/2
CSPP_BufEvent	= $012c/2
CSPP_RxMiss	= $0130/2
CSPP_TxCol	= $0132/2
CSPP_LineCtl	= $0112/2
CSPP_LineSt	= $0134/2
CSPP_SelfCtl	= $0114/2
CSPP_SelfSt	= $0136/2
CSPP_BusCtl	= $0116/2
CSPP_BusSt	= $0138/2
CSPP_TestCtl	= $0118/2
CSPP_IA		= $0158/2

;--------------------------------------

CS_RxCtl		= $0005
CS_RxCtl_IAHashA	= $0040
CS_RxCtl_PromiscousA	= $0080
CS_RxCtl_RxOkA		= $0100
CS_RxCtl_MulticastA	= $0200
CS_RxCtl_IndividualA	= $0400
CS_RxCtl_BroadcastA	= $0800
CS_RxCtl_CRCErrorA	= $1000
CS_RxCtl_RuntA		= $2000
CS_RxCtl_ExtradataA	= $4000

CS_LineCtl		= $13
CS_LineCtl_SerRxOn	= $40
CS_LineCtl_SerTxOn	= $80

;--------------------------------------
; Name:
;   cs8900a_rrnet_install
;
; Description:
;   Detect RRNet Adapter for the Retro Replay and install physical drivers on success.
;
; Parameters:
;   -
;
; Return:
;   Carry : 0 = RRNet detected and driver installed
;           1 = no RRNet found
;--------------------------------------
; Name:
;   cs_detect
;
; Description:
;   Look for crystal magic and version number.
;   The product id is located at packet page 0 - 3 and contains
;     $0e $63 $00 VV
;   where VV is the revision code of the chip
;     Rev B : $05
;     Rev C : $06
;     Rev D : $07
;   (what happened to Rev A?)
;
; Parameters:
;   -
;
; Returns:
;   Carry: 0 = ok
;          1 = error
;   A    : revision letter (b, c, d) if ok

cs8900a_rrnet_install .proc
	; look for rrnet
        .if RR_NET
	lda $de01
	and #%10111000
	ora #%00000001
	sta $de01
        .fi

	lda #CSPP_PId			;check the first ID register
	jsr cs_readPage
	cpx #<$630e
	bne err_noMagicFound
	cpy #>$630e
	bne err_noMagicFound

        lda #CSPP_PId+1			;check the second ID register
	jsr cs_readPage
	cpx #0
        bne err_noMagicFound

	; ok, found -> install the jump vectors
	lda #<cs_open
	sta PhyJmp_Open+1
	lda #>cs_open
	sta PhyJmp_Open+2

	lda #<cs_unconfigure
	sta PhyJmp_Close+1
	lda #>cs_unconfigure
	sta PhyJmp_Close+2

	lda #<cs_getPacket
	sta PhyJmp_GetPacket+1
	lda #>cs_getPacket
	sta PhyJmp_GetPacket+2

	lda #<cs_sendPacket
	sta PhyJmp_SendPacket+1
	lda #>cs_sendPacket
	sta PhyJmp_SendPacket+2

	clc
	rts

err_noMagicFound
	sec
	rts
    .pend

;--------------------------------------
; Name:
;   cs_open
;
; Description:
;   Configure the chip and wait for link pulses.
;
; Parameters:
;   -
;
; Returns:
;   Carry : 0 = link pulses detected
;           1 = error

cs_open .proc
	jsr cs_configure
	bcs error

	ldx #<CsZp_TimeOutBuffer
	ldy #>CsZp_TimeOutBuffer
	jsr timer_get

waitForLinkPulses
	jsr cs_testLinkStatus
	bcc linkOk

	lda #CsCfg_OpenTimeout
	ldx #<CsZp_TimeOutBuffer
	ldy #>CsZp_TimeOutBuffer
	jsr timer_check
	bcc waitForLinkPulses

no_link
error
	sec
	rts

linkOk
	clc
	rts
    .pend


;--------------------------------------
; Name:
;   cs_configure
;
; Description:
;   Configure the chip.
;   Only individual packets (for the configured mac address) and
;   broadcast packets are accepted.
;
; Parameters:
;   -
;
; Returns:
;   -

cs_configure .proc
	;reset chip to assure the default values in all regs
	jsr cs_reset

	;enable receiving and transmission
	lda #CSPP_LineCtl
	ldx #<(CS_LineCtl | CS_LineCtl_SerRxOn | CS_LineCtl_SerTxOn)
	ldy #>(CS_LineCtl | CS_LineCtl_SerRxOn | CS_LineCtl_SerTxOn)
	jsr cs_writePage

	;accept individual and broadcast packets
	lda #CSPP_RxCtl
	ldx #<(CS_RxCtl | CS_RxCtl_RxOkA | CS_RxCtl_IndividualA | CS_RxCtl_BroadcastA)
	ldy #>(CS_RxCtl | CS_RxCtl_RxOkA | CS_RxCtl_IndividualA | CS_RxCtl_BroadcastA)
	jsr cs_writePage

	;set the mac address
	lda #CSPP_IA
	ldx PhyZp_MacAdr
	ldy PhyZp_MacAdr+1
	jsr cs_writePage
	lda #CSPP_IA+1
	ldx PhyZp_MacAdr+2
	ldy PhyZp_MacAdr+3
	jsr cs_writePage
	lda #CSPP_IA+2
	ldx PhyZp_MacAdr+4
	ldy PhyZp_MacAdr+5
	jmp cs_writePage
    .pend

;--------------------------------------
; Name:
;   cs_unconfigure
;
; Description:
;   Unconfigure the chip. No packets can be transmitted or received.
;
; Parameters:
;   -
;
; Returns:
;   -

cs_unconfigure = cs_reset


;--------------------------------------
; Name:
;   cs_getPacket
;
; Description:
;   Get a received packet from the chip's buffer.
;   The routine immediately returns if the buffer is empty.
;
; Parameters:
;   -
;
; Returns:
;   Carry : 0 = ok
;           1 = error
;   X     : $ff = no packet waiting
;           else = BucketNr holding the received data

cs_getPacket .proc
	; test the link status
;       jsr cs_testLinkStatus
 ;      bcs nopacket

	; select the receive event register
	lda #<(CSPP_RxEvent*2)
	sta CS_PacketPage
	lda #>(CSPP_RxEvent*2)
	sta CS_PacketPage+1

	; test bit #8 (valid packet received)
	lda CS_PacketData0+1
	ldy CS_PacketData0
	lsr
	bcc nopacket
	; skip status
	lda CS_RxTxPort0+1
	lda CS_RxTxPort0

	; read packet length and allocate bucket
	ldy CS_RxTxPort0+1
	lda CS_RxTxPort0
	; round the number of bytes up to next 16 bit boundary
        adc #1-1
        and #$fe
        bcc +
        iny
+	sta CsZp_PacketLen
	sty CsZp_PacketLen+1
	jsr Bucket_Alloc
	bcs bucketError
	stx CsZp_Bucket

	; read the packet
	lda CsZp_PacketLen+1
	beq transrest
-       ldx #128
	jsr transfer
	inc BucketZp_BufPtr+1
	dec CsZp_PacketLen+1
	bne -

transrest
	lda CsZp_PacketLen
	beq transdone
        lsr
        tax
	jsr transfer
transdone

	ldx CsZp_Bucket
	clc
	rts

bucketError
	sec
	rts

nopacket
	ldx #$ff
	clc
	rts

transfer
        ldy #0
-	lda CS_RxTxPort0
	sta (BucketZp_BufPtr),y
	iny
	lda CS_RxTxPort0+1
	sta (BucketZp_BufPtr),y
	iny
        dex
	bne -
        rts

    .pend

;--------------------------------------
; Name:
;   cs_sendPacket
;
; Description:
;   Send a packet.
;
; Sarameters:
;   x : number of bucket to send
;
; Returns:
;   Carry : 0 = ok
;           1 = error

cs_sendPacket .proc
	stx CsZp_Bucket

	; test the link status
	jsr cs_testLinkStatus
	bcc linkOk
	; no link -> unable to send anything
	rts

linkOk
	; buffer to send
	ldx CsZp_Bucket
	jsr Bucket_GetAdr

	; ask for bufferspace
	lda #$c9
	sta CS_TxCmd
	lda #0
	sta CS_TxCmd+1

	; buffer length
	jsr Bucket_Len
        clc
        adc #1
        and #$fe
	sta CsZp_PacketLen
        sta CS_TxLength
        tya
        adc #0
	sta CsZp_PacketLen+1
	sta CS_TxLength+1

	; select bus status register
	lda #$38
	sta CS_PacketPage
	lda #1
	sta CS_PacketPage+1

	; wait until the requested space is free
	;TODO: a timeout would be nice
waitforspace
	lda CS_PacketData0+1
	ldx CS_PacketData0
	lsr
	bcc waitforspace

	; send the block
	lda CsZp_PacketLen+1
	beq transrest
-       ldx #128
	jsr transfer
	inc BucketZp_BufPtr+1
	dec CsZp_PacketLen+1
	bne -

transrest
	lda CsZp_PacketLen
	beq transdone
        lsr
        tax
	jsr transfer
transdone
	ldx CsZp_Bucket
	jmp Bucket_Free

transfer
	ldy #0
-	lda (BucketZp_BufPtr),y
	sta CS_RxTxPort0
	iny
	lda (BucketZp_BufPtr),y
	sta CS_RxTxPort0+1
	iny
	dex
	bne -
	rts
    .pend

;--------------------------------------
; Name:
;   cs_readPage
;
; Description:
;   Read the packet page register (a*2) to x(lo) and y(hi)
;
; Parameters:
;   A : number of the packet page register divided by 2
;
; Returns:
;   X : lo byte of Packet Page register contents
;   Y : hi byte of Packet Page register contents

cs_readPage
	asl
	sta CS_PacketPage
	lda #0
	rol
	sta CS_PacketPage+1
	ldx CS_PacketData0
	ldy CS_PacketData0+1
	rts

;--------------------------------------
; Name:
;   cs_writePage
;
; Description:
;   Write x(lo) and y(hi) to the packet page register (a*2)
;
; Parameters:
;   A : number of the packet page register divided by 2
;   X : lo byte of the data to write to the Packet Page register
;   Y : hi byte of the data to write to the Packet Page register
;
; Returns:
;   -

cs_writePage
	asl
	sta CS_PacketPage
	lda #0
	rol
	sta CS_PacketPage+1
	stx CS_PacketData0
	sty CS_PacketData0+1
	rts

;--------------------------------------
; Name:
;   cs_testLinkStatus
;
; Description:
;   Test the line status, i.e. if a device is connected to the other end of the cable.
;
; Parameters:
;   -
;
; Returns:
;  Carry : 0 = link pulses detected (== device present)
;        : 1 = no link puses

cs_testLinkStatus
	lda #CSPP_LineSt
	jsr cs_readPage

	; invert bit #7 and move it to carry
	txa
	eor #$80
	asl
	rts

;--------------------------------------
; Name:
;   cs_reset
;
; Description:
;   Reset the chip and wait for reset finished.
;
; Parameters:
;   -
;
; Returns:
;   -

cs_reset .proc
	;reset chip to assure the default values in all regs
	lda #CSPP_SelfCtl
	ldx #<$0055
	ldy #>$0055
	jsr cs_writePage

	; wait until init is done, bit #7 of self Status == 1 signals this
waitForResetDone
	lda #CSPP_SelfSt
	jsr cs_readPage
	txa
	bpl waitForResetDone

	rts
    .pend

;--------------------------------------


;    .)


