
;--------------------------------------

;	.segment "data"
	.comment
UdpZp_Checksum			.word 0

UdpZp_SrcPort			.word 0
UdpZp_DstPort			.word 0

UdpZp_BucketIn			.byte 0
UdpZp_BucketOut			.byte 0
UdpZp_BucketChecksumIn		.byte 0
UdpZp_BucketChecksumPsy		.byte 0

UdpZp_SrcIpIdx			.byte 0
UdpZp_DstIpIdx			.byte 0
UdpZp_ExpectedIpIdx		.byte 0

UdpZp_PacketLength		.word 0

UdpZp_ChkSumPacketLength	.word 0

UdpZp_TmpIp0			*=*+4
UdpZp_TmpIp1			*=*+4
	.endc
;--------------------------------------

;	.segment "bank0"

UdpHeader_SrcPort		= $00
UdpHeader_DstPort		= $02
UdpHeader_Length		= $04
UdpHeader_Checksum		= $06
UdpHeader_len			= $08

UdpPsyHeader_SrcIp		= $00
UdpPsyHeader_DstIp		= $04
UdpPsyHeader_ZeroDummy		= $08
UdpPsyHeader_Protocol		= $09
UdpPsyHeader_Length		= $0a
UdpPsyHeader_len		= $0c

;--------------------------------------

udp_setSrcPort
	sta UdpZp_SrcPort
	stx UdpZp_SrcPort+1
	rts

;--------------------------------------

udp_setDstPort
	sta UdpZp_DstPort
	stx UdpZp_DstPort+1
	rts

;--------------------------------------
; Send udp datagram
; non constant header fields are set
; these are
;  udp and ip length, src and dst ip
;  and udp and ip checksum
; parameters are
;  x - bucket nr

udp_send .proc
	stx UdpZp_BucketIn
	sty UdpZp_DstIpIdx

	; source ip is me
	lda MyIpIdx
	sta UdpZp_SrcIpIdx

	lda #UdpHeader_len
	jsr Bucket_GrowFront_small
	bcs error
	stx UdpZp_BucketOut

	jsr Bucket_GetAdr

	jsr Bucket_Len
        pha
        tya

	ldy #UdpHeader_Length
	sta (BucketZp_BufPtr),y
	iny
	pla
	sta (BucketZp_BufPtr),y

        ldy #UdpHeader_DstPort+1
	; set destination port
	lda UdpZp_DstPort
	sta (BucketZp_BufPtr),y
	dey
	lda UdpZp_DstPort+1
	sta (BucketZp_BufPtr),y
	dey

	; set source port
	lda UdpZp_SrcPort
	sta (BucketZp_BufPtr),y
	dey
	lda UdpZp_SrcPort+1
	sta (BucketZp_BufPtr),y

	; generate checksums
	jsr udp_outChecksum

	;send the packet
	lda #IpV4_Protocol_Udp
	ldy UdpZp_DstIpIdx
	jmp ipv4_send

error
	sec
	rts
    .pend

;--------------------------------------

udp_get .proc
	sty UdpZp_ExpectedIpIdx
	jmp ipv4_get
    .pend

;--------------------------------------
; Get udp datagram

udp_getPacket .proc
	stx UdpZp_BucketIn
	sty UdpZp_SrcIpIdx

	; get source ip of packet
	lda #<UdpZp_TmpIp0
	ldx #>UdpZp_TmpIp0
	jsr ipCache_Get

	; get expected ip (set by udp_get which acts like connect)
	ldy UdpZp_ExpectedIpIdx
	lda #<UdpZp_TmpIp1
	ldx #>UdpZp_TmpIp1
	jsr ipCache_Get

	;source ip adr must be the expected ip
	ldx #3
-	lda UdpZp_TmpIp0,x
	cmp UdpZp_TmpIp1,x
	bne error
	dex
	bpl -

	; destination ip is me (already checked in ipv4 layer)
	lda MyIpIdx
	sta UdpZp_DstIpIdx

	ldx UdpZp_BucketIn
	jsr Bucket_GetAdr

	ldy #UdpHeader_DstPort
	lda (BucketZp_BufPtr),y
	cmp UdpZp_SrcPort+1
	bne udp_badPacket
	iny
	lda (BucketZp_BufPtr),y
	cmp UdpZp_SrcPort
	bne udp_badPacket

	; generate checksums
	jsr udp_testInChecksum
	bcs error

	; free incoming ip
	ldy UdpZp_SrcIpIdx
	jsr ipCache_Free

	; cut off the udp header
	ldy #UdpHeader_len
	jsr Bucket_ShrinkFront
	bcs error2

	jmp dns_getPacket

error
udp_badPacket
	ldx DnsZp_BucketIn
	jsr Bucket_Free
	ldy UdpZp_SrcIpIdx
	jsr ipCache_Free
error2
	sec
	rts
    .pend


;--------------------------------------
; the udp checksum includes a virtual ip
; header with
;  * src ip (4 bytes)
;  * dst ip (4 bytes)
;  * zero (1 byte)
;  * protocol typ (1 byte)
;  * udp packet length (2 bytes)
; to enable checksum generation in one row,
; the ip checksum is set to the udp packet length
; and the udp checksum is set to 0

udp_outChecksum .proc
                stx UdpZp_BucketChecksumIn

                jsr Bucket_Len
                sta UdpZp_ChkSumPacketLength
                sty UdpZp_ChkSumPacketLength+1

                ; allocate psy header
                lda #<UdpPsyHeader_len
                sta MakeChecksumZp_Len
                ldy #>UdpPsyHeader_len
                sty MakeChecksumZp_Len+1
                jsr Bucket_Alloc
                bcs psy_error

                ; create the psy header
                stx UdpZp_BucketChecksumPsy
                ldy #UdpPsyHeader_len-1
                lda UdpZp_ChkSumPacketLength
                sta (BucketZp_BufPtr),y
                dey
                lda UdpZp_ChkSumPacketLength+1
                sta (BucketZp_BufPtr),y
                dey
                lda #IpV4_Protocol_Udp
                sta (BucketZp_BufPtr),y
                dey
                lda #0
                sta (BucketZp_BufPtr),y

                ; copy source ip to temp packet
                ldy UdpZp_SrcIpIdx
                lda BucketZp_BufPtr
                sta MakeChecksumZp_Ptr
                ldx BucketZp_BufPtr+1
                stx MakeChecksumZp_Ptr+1
                jsr ipCache_Get
                bcs psy_error

                ; copy destination ip to temp packet
                ldy UdpZp_DstIpIdx
                lda #4
                jsr ipCache_Get_tobucket
                bcs psy_error

                ldx UdpZp_BucketChecksumIn
                jsr Bucket_GetAdr

                jsr MakeChecksum

                ldy #UdpHeader_Checksum
                eor #$ff
                sta (BucketZp_BufPtr),y
                iny
                txa
                eor #$ff
                sta (BucketZp_BufPtr),y

                ldx UdpZp_BucketChecksumPsy
                jsr Bucket_Free

                ldx UdpZp_BucketChecksumIn
                jsr Bucket_Len
                sta MakeChecksumZp_Len
                sty MakeChecksumZp_Len+1

                jsr Bucket_GetAdr
                sta MakeChecksumZp_Ptr
                sty MakeChecksumZp_Ptr+1

                jsr MakeChecksum

                ldy #UdpHeader_Checksum
                sta (BucketZp_BufPtr),y
                iny
                txa
                sta (BucketZp_BufPtr),y

                ldx UdpZp_BucketChecksumIn
                clc
                rts

psy_error
		sec
		rts
    		.pend

;--------------------------------------

udp_testInChecksum .proc
	ldy #UdpHeader_Checksum
	lda (BucketZp_BufPtr),y
	sta UdpZp_Checksum
	iny
	lda (BucketZp_BufPtr),y
	sta UdpZp_Checksum+1

	jsr udp_outChecksum
	bcs error

	ldy #UdpHeader_Checksum
	lda (BucketZp_BufPtr),y
	cmp UdpZp_Checksum
	bne udp_badInChecksum
	iny
	lda (BucketZp_BufPtr),y
	eor UdpZp_Checksum+1
        cmp #1
	rts

error
udp_badInChecksum
	sec
	rts
    .pend

;--------------------------------------


