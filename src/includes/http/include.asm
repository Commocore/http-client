
;#segdef "zp", $57-$72
;#segdef "zp2", $57-$72
;#segdef "buckets", $a000-$b000
;#segdef "data", $b000-$c000
;#segdef "cfgLowData", $0340-$0400

;#segdef "bank0", $e000-$ff10
;#segdef "bank1", $8000-$a000

;#outfile @, startadr, sort, $00, "bank1", "bank0"

; the code is not running in a retro replay bank
;RetroReplay = 0

	;.assert CFG_ROM_E000, 0, 0

;--------------------------------------
; init the startadr of all segments

;	*=$340

;	.include "s_cfgLowData.asm"
old
;	.segment "zp"
;Status			*=*+1
;System_CSRevision	*=*+1
        .include "s_zp.asm"

;	.segment "zp2"
;	* = $57
;        .include "s_zp2.asm"

;	.segment "buckets"
;	* = $a000
;        .include "s_buckets.asm"

;	.segment "data"
 ;       * = $bf00
;        .include "s_data.asm"

;	.segment "bank0"
;	* = $e000

;	.segment "bank1"

;--------------------------------------

ramLoadBuf_len			= $f0


STATUS_TimeOut		= $04
STATUS_IllegalPacket	= $08
STATUS_ChecksumError	= $10

;--------------------------------------

;	.segment "bank1"

;--------------------------------------

.include "errorcodes.inc"
;.include "errorcodes.asm"



;--------------------------------------

;.include "settings.asm"

;		*=$e000

                ;.assert CFG_RAM_E000, CFG_RAM_E000, CFG_ROM_E000

.include "basic_bank0.asm"

.include "memcopy.asm"

; old and slow, but reliable
;.include "buckets.src"
; new and fast, but not tested yet
.include "buckets2.asm"

.include "filename.asm"
.include "ipCache.asm"
.include "timer.asm"

.include "checksum.asm"
.include "cs8900a.asm"
.include "phy.asm"
.include "eth.asm"
.include "ipv4.asm"
.include "arp.asm"
;.include "pppoe.src"
;.include "ppp.src"
.include "udp.asm"
.include "tcp.asm"
.include "http.asm"
.include "dns.asm"



