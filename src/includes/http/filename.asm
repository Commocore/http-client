
;--------------------------------------

;	.segment "data"
		.comment
getBNumZp_Len	.byte 0
getBNumZp_Val	.byte 0
inputPos	.byte 0

HostNameLength	.byte 0
DnsZp_HostNameBuf .byte 0

TempIp		*= *+4

getBNumBuf	*= *+3
		.endc

;	.segment "bank0"

;--------------------------------------
; parse routines

checkSlash	.proc
		lda #"/"
		.byte $2c
		.cerror checkDot & 0
		.pend

checkDot	.proc
		lda #"."
		.cerror checkChar & 0
		.pend

checkChar	.proc
		ldy inputPos
		cmp fileName,y
		bne checkComma_err
		inc inputPos
		clc
		rts

checkComma_err
		sec
		rts
		.pend

getIp		.proc
		jsr getBNum
		bcs getIp_err
		sta TempIp
		jsr checkDot
		bcs getIp_err
		jsr getBNum
		bcs getIp_err
		sta TempIp+1
		jsr checkDot
		bcs getIp_err
		jsr getBNum
		bcs getIp_err
		sta TempIp+2
		jsr checkDot
		bcs getIp_err
		jsr getBNum
		bcs getIp_err
		sta TempIp+3
getIp_err
		rts
		.pend

getBNum
		ldx #0
		ldy inputPos
getBNum0
		lda fileName,y
		cmp #"0"
		bcc getBNum1
		cmp #"9"+1
		bcs getBNum1
		and #$0f
		sta getBNumBuf,x
		iny
		inx
		cpx #4
		bcc getBNum0
getBNum_err
		sec
		rts

getBNum1
		dex
		bmi getBNum_err
		stx getBNumZp_Len
		sty inputPos
		lda #0
		sta getBNumZp_Val

		ldx #0
getBNum2
		ldy getBNumZp_Len
		lda getBNumBuf,y
		tay
		beq getBNum4
getBNum3
		lda getBNumZp_Val
		clc
		adc getBNumTab,x
		sta getBNumZp_Val
		bcs getBNum_err
		dey
		bne getBNum3
getBNum4
		inx
		dec getBNumZp_Len
		bpl getBNum2

		lda getBNumZp_Val
		clc
		rts

getBNumTab
		.byte 1, 10, 100

;--------------------------------------
; Get hostname
;
; First try ip, then hostname
;
; TODO: parse filename for absolute or relative path before the filename
; for now only absolute paths are possible


parseHostName	.proc

	; start at the beginning of the filename
		lda #0
		sta inputPos

		jsr parseIpHost
		bcs tryName
		rts

tryName
	; start at the beginning of the filename
		lda #0
		sta inputPos

		jmp parseNameHost
		.pend


parseIpHost	.proc
	; try to read in an ip (xxx.xxx.xxx.xxx)
		jsr getIp
		bcs parseError

	; ok, save the ip
		lda #<TempIp
		ldx #>TempIp
		jsr ipCache_New
		sty DstIpIdx
		bcs error

	; remember position of slash
		ldy inputPos
		sty HostNameLength
		cpy fileName_len
		blt +
		lda #"/"
		sta fileName,y
		inc fileName_len

	; test for slash after the ip
+		jmp checkSlash

error
parseError
		rts

		.pend


;--------------------------------------


parseNameHost	.proc
		ldy #0
		lda #"/"
-		cmp fileName,y
		beq getHost2
		iny
		cpy fileName_len
		blt -
		sta fileName,y
		inc fileName_len

getHost2
	; cut hostname in '.' separated pieces
	;first alloc a buffer
		sty HostNameLength

	; generate dns query
	; TODO: move this to dns.src
		tya
		ldy #0
		sec			; +1
		adc #dnsReq_fixedPostData_len
		bcc getHost3
		iny
getHost3
		jsr Bucket_Alloc
		bcs error
		stx DnsZp_HostNameBuf

		ldy HostNameLength
		ldx #0
splitHostName
		lda fileName-1,y
		cmp #"."
		bne +
		txa
		ldx #$ff
+		sta (BucketZp_BufPtr),y
		inx
		dey
		bne splitHostName
		txa
		sta (BucketZp_BufPtr),y

	; move pointer behind encoded host name
		lda BucketZp_BufPtr
		sec			; +1
		adc HostNameLength
		sta BucketZp_BufPtr
		bcc +
		inc BucketZp_BufPtr+1
+

	; copy the fixed data (type and class)
		ldy #dnsReq_fixedPostData_len-1
copyFixedPostDnsReq
		lda dnsReq_fixedPostData,y
		sta (BucketZp_BufPtr),y
		dey
		bpl copyFixedPostDnsReq

		ldx DnsZp_HostNameBuf
		jsr dns_getIp
		ldx DnsZp_HostNameBuf
		jsr Bucket_Free
		clc
		rts
error
		sec
		rts

dnsReq_fixedPostData
		.byte $00		; end of hostname
		.byte $00, $01		; type (host adr)
		.byte $00, $01		; class (inet)
dnsReq_fixedPostData_len = * - dnsReq_fixedPostData

		.pend


