;--------------------------------------
; Errorcodes
;
; This file contains a global list of all errorcodes used by the project.
; It will be used to print some messages (soon).
;--------------------------------------

;All ok, no error
Error_Ok				= 0

;No magic bytes were found. There might be no CS8900a Chip.
Error_NoCS8900aMagicFound		= 1

;The first 3 bytes of the magic were ok, but the version number is not known (new revision of the chip?!?)
Error_UnknownCS8900aVersionNumber	= 2

Error_Cfg_Ip				= 3
Error_MissingComma			= 4
Error_MissingDot			= 5
Error_MissingSlash			= 6

Error_OutOfBucketMemory			= 7
Error_IllegalBucketIndex		= 8

Error_Timeout				= 9

Error_TcpConnectionRefused		= 10

Error_LinkDown				= 11

Error_Phy_OpenDriver			= 12
Error_Phy_UnknownDriverIdx		= 13
Error_Phy_IllegalOperationInState	= 14

