﻿
; Resets length of the body, use it before each request
resetBody .macro
	lda #0
	sta HttpPostMethod.requestBodySize + 0
	sta HttpPostMethod.requestBodySize + 1
	sta HttpPostMethod.requestBodySizeInternal + 0
	sta HttpPostMethod.requestBodySizeInternal + 1
.endm


; @return A - size lo-byte
; @return X - size hi-byte
getBodyLength .macro
	lda HttpPostMethod.requestBodySize + 0
	ldx HttpPostMethod.requestBodySize + 1
.endm


; @param word memoryLocation
setBodyMemoryLocation .macro memoryLocation
	ldx #<\memoryLocation
	ldy #>\memoryLocation
	jsr HttpPostMethod.initBodyMemoryLocation
.endm


; @param word stringLocation
; @param byte length
addStringToBody .macro stringLocation, length
	lda #<\stringLocation
	sta HttpPostMethod.copyStringPointer + 1
	lda #>\stringLocation
	sta HttpPostMethod.copyStringPointer + 2
	ldy #\length
	jsr HttpPostMethod.addStringToBodyRequest
.endm


addByteToBody .macro
	jsr HttpPostMethod.addByteToBodyRequest
.endm
