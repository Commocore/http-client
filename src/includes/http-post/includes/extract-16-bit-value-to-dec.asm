
; @access public
; @uses numberLo
; @uses numberHi
; @uses newNumberLo
; @uses newNumberHi
; @uses number_1
; @uses number_10
; @uses number_100
; @uses number_1000
; @uses number_10000
; @return void
extract16bitValueToDec
	lda #0
	sta number10000
	sta number1000
	sta number100
	sta number10
	sta number1

	; tens of thousands
	sec
-
	lda numberLo
	sbc #<10000
	sta newNumberLo
	lda numberHi
	sbc #>10000
	sta newNumberHi
	
	bcc +
		lda newNumberLo
		sta numberLo
		lda newNumberHi
		sta numberHi
		inc number10000
		jmp -
+
	
	; thousands
	sec
-
	lda numberLo
	sbc #<1000
	sta newNumberLo
	lda numberHi
	sbc #>1000
	sta newNumberHi
	
	bcc +
		lda newNumberLo
		sta numberLo
		lda newNumberHi
		sta numberHi
		inc number1000
		jmp -
+

	; hundrers
	sec
-
	lda numberLo
	sbc #<100
	sta newNumberLo
	lda numberHi
	sbc #>100
	sta newNumberHi
	
	bcc +
		lda newNumberLo
		sta numberLo
		lda newNumberHi
		sta numberHi
		inc number100
		jmp -
+

	; tens
	sec
-
	lda numberLo
	sbc #<10
	sta newNumberLo
	bcc +
		lda newNumberLo
		sta numberLo
		inc number10
		jmp -
+

	; ones
	clc
	lda numberLo
	sta number1
rts
