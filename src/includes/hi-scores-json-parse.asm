
; @access public
; @return void
hiScoresJsonParse
	#setStreamedJsonObject
	#setPetsciiMode
	
	#setJsonOutputMemoryLocation playersTableLocation, SCREEN_LOCATION + 40 * 4
	#setJsonOutputMemoryLocation valuesTableLocation, SCREEN_LOCATION + 40 * 4 + 20
	
	jsr readFromStream
-
	#expectJsonKey "player", setPlayer
	#expectJsonKey "value", setValue
	#isJsonObjectCompleted
	bne -
	
	#isJsonArrayCompleted
	bne -
rts


; @access private
; @return void	
setPlayer
	#storeJsonValue playersTableLocation, 40
rts


; @access private
; @return void	
setValue
	#storeJsonValue valuesTableLocation, 40
rts


playersTableLocation
	.word 0
	
	
valuesTableLocation
	.word 0
	