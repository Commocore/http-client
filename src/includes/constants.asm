
; HTTP client settings
MOCK_HTTP_REQUEST = 0
DEBUG_MODE = 1
REQUEST_METHOD = 'GET'
HTTP_SEND_POST_BUFFER_TEST_LOCATION = 0 ; $0400 ; Handy to debug POST requests built in bucket by allocation the buffer in any location

; HTTP-Load library settings
RR_NET = 1
C128 = 0
SUPERRAM = 0 ; swap on SUPERRAM (16MB)

VDC = 0
VDC_64K = 0

; Memory configuration
SCREEN_LOCATION = $0400
Bucket_MemLen	= $0600
Bucket_MemStart	= $c400

CS8900_BASE     = $de00

CFG_RAM_E000	= %00000001
CFG_ROM_E000	= %00000010

; Kernal commands
KERNAL_SETLFS = $ffba
KERNAL_SETNAM = $ffbd
KERNAL_OPEN = $ffc0
KERNAL_CLOSE = $ffc3
KERNAL_CHKIN = $ffc6
KERNAL_CHRIN = $ffcf
KERNAL_CLRCHN = $ffcc
