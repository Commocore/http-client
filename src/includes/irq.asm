
irq
	pha        ;store register A in stack
	txa
	pha        ;store register X in stack
	tya
	pha        ;store register Y in stack

	inc $d019

	; Indicate that IRQ runs properly
	inc $0400 + 999
	
	pla
	tay
	pla
	tax
	pla
rti
